John David Moffa
Dr. Steinmetz
CSC 450
18 September 2018



Project 1 Description: Reading Data from a text 
file/text document and counting how 
many times each word and punctuation 
is used in a text document.

Change Log:

Classes:

Added Main Class class:  WordCounter1
Added Global class in WordCounter1: FunctionCounter
Added Class: WasScanned

WordCounter1 Functions:

DocumentScanner: takes in a string as input for the file name
StealingTheGoldFromTheDragon: Takes in a Map object and a Object
WordBuilder: takes in a String and an int as inputs
IsDelimiter: takes in a string and an int as inputs
CommandScanner: takes in no arguments and returns a string
Main: Runs the program

WasScanned Functions (getters && setters):

getStartPi
getEndPi
getName
getType

setStartPi
setEndPi
SetName
SetScanType

See SDD PDF or word doc for more detail about this project.


What was added:

Static class WindowGUI that is was integrated into the main class WordCounter1.

What does WindowGUI do?

WindowGUI produces a small one button GUI window on the users screen, the button allows the
user to open as many text fies as they want until they get bored and exits the GUI window. 
The user can exit the program by  using the exit button in the upper right-hand corner 
(Windows and Debian derivatives) or the exit button will be in the upper left-hand 
corner (MacOS and other Unix based OS).  The might exit by using keyboard shortcuts, 
alt + F4 for Windows and Debian OS derivatives, CMD + W for Mac and other Unix based OS.

