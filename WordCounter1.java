/**
 *John David Moffa
 *Dr. Stienmetz
 *CSC 450
 *
 *Version 27
 *
 *Added the following functionality:
 *-Works with command line input
 *-Remove punctuation
 *-Output is ordered
 *-Added GUI
 *-Output is no longer sent to console and is instead sent to a text file.
 */
package wordcounter1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
//importing the hashMap, Map, Set, and Iterator classes
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
//importing classes that deal with input handling, output handling,
//and file handling.
import java.io.File;


import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author John David Moffa
 *
 * Class WordCounter1 holds the following Function: main Function:
 */
public class WordCounter1 {

    /**
     * Main function of the program and allows the program to run and outputs a
     * message to the user via the the use of a GUI window and 
     * an output text file
     *
     * @param args is the array of arguments 
     * that can be used to set up a scanner.
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        //Runs the entire program by calling this one function.
        window();
    }

    /**
     * Scans in data from a document given to it by the user via the function
     * CommandScanner. The DucumentScanner creates the hash-map, keys, and
     * values; and increments the counters from the FunctionCounter class.
     *
     * @param fileSentByUser is the file sent in by the user to be scanned
     * @throws java.io.FileNotFoundException
     */
    public static void documentScanner(String fileSentByUser) throws FileNotFoundException {

        //creating a new hashmap called treasureMap using the hashmap class
        HashMap<Integer, String> treasureMap = new HashMap<Integer, String>();

        //creating an array of size 10000
        //the index into count is the key of the hash
        int[] count = new int[10000];
        /*
         creating a file object using the file class,
        this is file that is to be scanned
         */
        File file;

        // using the newly created file object to open a file
        file = new File(fileSentByUser);

        //scanning the text of the opened file
        Scanner reader = new Scanner(file);

        //crea
        String buffer;
        //start the hash here here at at zero.
        int words = 0;

        /* 
         after scanning the first line, 
         keep scanning line by line, 
         if the next line exists.
        
         if the next line doens't exist, then exit
         */
        while (reader.hasNextLine()) {

            //move onto the next line if one exists
            buffer = reader.nextLine();
            //System.out.println( buffer);
            for (int i = 0; i < buffer.length();) {
                WasScanned was = wordBuilder(buffer, i);

                //First check if this word is already stored in the hasmap
                //if it's not already inside the hashmap, 
                //we create a new word and a new key
                boolean flag = treasureMap.containsValue(was.getName());

                // Increase the amount of words of words
                if (!flag) {
                    /* this is the key for the value int the hashmap that 
                     associated with this word.
                    
                     this is garunteed to be unique becuase it is incrimented
                     for every new word.*/
                    ++words;

                    // Put the new word into the HashMap 
                    treasureMap.put(words, was.getName());
                    // Increase the count of the number of total words found
                    count[words] = 1;
                } else {
                    //if this isn't a new word, 
                    //increment the current count at the key location
                    //inside the count array.
                    //computer the index of the array counter
                    ++count[(int) stealingTheGoldFromTheDragon(treasureMap, was.getName())];
                }
                // the start of the next scan is set by WasScanned
                i = was.getEndPi();
            }
        }

        /*
         Print out the contents of the hashMap that uses a 2 arrays 
         the key to the hashmap (the key)
         valueOfTheObject       (the value)
         */
        Set set = treasureMap.entrySet();
        Iterator index = set.iterator();
        while (index.hasNext()) {
            HashMap.Entry treasureIsland = (HashMap.Entry) index.next();
            System.out.println("This word occurs "
                    + count[(int) treasureIsland.getKey()]
                    + " times.\t Key is " + treasureIsland.getKey()
                    + " and Value is: " + treasureIsland.getValue());
        }

        //reporttinng to the user the total amount of words 
        //and total amount of delimiters
        System.out.println("The total amount of words is: "
                + FunctionCounter.wordCounter + " words.");

        System.out.println("The total amount of delimiters is: "
                + FunctionCounter.delimiterCounter + " delimiters.");

        /*
         *This is a list sort that prints words based on usage
         * from lest frequent usage to most frequent usage. 
         */
        for (int i = 1; i < FunctionCounter.wordCounter; ++i) {
            set = treasureMap.entrySet();
            index = set.iterator();
            while (index.hasNext()) {
                HashMap.Entry treasureIsland = (HashMap.Entry) index.next();
                if (count[(int) treasureIsland.getKey()] == i) {
                    System.out.println(count[(int) treasureIsland.getKey()]
                            + "\t" + treasureIsland.getValue());
                }
            }
        }
    }

    /**
     * This finds a key from any given value inside the hash table
     *
     * @param theMap is the HashMap of the words
     * @param valueOfObject is the key to each word
     * @return null if the object hasn't been stolen
     */
    public static Object stealingTheGoldFromTheDragon(Map theMap, Object valueOfObject) {
        
        //Go through all hash entries
        for (Object objectStolen : theMap.keySet()) {
            //if a match is found, return the key(objectStolen)
            if (theMap.get(objectStolen).equals(valueOfObject)) {
                return objectStolen;
            }
        }
        return null;
    }

    /**
     * WordBuilder builds words using a character array and pointers.
     *
     * @param buffer is the size limit of a string
     * @param pi is the position in a string
     * @return the start position, end position, word, and whether the
     * current character is a delimiter or not.
     */
    public static WasScanned wordBuilder(String buffer, int pi) {
        int i;
        WasScanned was = new WasScanned();
        // sandbox builds a string with a maximum character size of 1024
        StringBuilder sandbox = new StringBuilder(1024);

        // passing in the buffer and the position of the string pointer
        if (isDelimiter(buffer, pi)) {
            i = pi;// index i is equal to the pointer position
            was.setScanType(0); // delimiter is 0
            ++FunctionCounter.delimiterCounter;
            //get the character in buffer posion pi.
            sandbox.append(buffer.charAt(pi));
            //after reaching the end of a word, move onto the next word.

            //for start of next scan
            was.setEndPi(pi + 1);

        } else {
            was.setScanType(1); // word is 1
            ++FunctionCounter.wordCounter;
            for (i = pi; i < buffer.length() && !isDelimiter(buffer, i); i++) {

                //get the character in buffer posion pi
                sandbox.append(buffer.charAt(i));

            }
            // set the end position of the string, for the start of next string
            was.setEndPi(i);
        }

        was.setStartPi(pi);

        was.setName(sandbox.toString());

        if (i >= buffer.length()) {
            was.setScanType(2); // end of buffer ( like a delimiter )
        }
        return (was);
    }

    /**
     * Looks to see if a character is a delimiter or a word.
     *
     * @param buffer is the string to look at
     * @param pi is the position in the string
     * @return false a word is found and true if a delimiter is found.
     */
    public static boolean isDelimiter(String buffer, int pi) {
        int val = (int) buffer.charAt(pi);

        /*
         Function IsDelimiter checks the text 
         file for punctuation and and grammer
         */
        // this is cheching for apostraphies, then it's part of a word.
        if (val == 39) {
            return false;
        }

        /* 
         Checking for ascii values of characters below the 48th character (0)
         on the ascii table.
         */
        if (val >= 0 && val < 48) {
            return true;
        }

        //checking for all the capital letters in the text (A - Z)
        if (val > 57 && val < 65) {
            return true;
        }

        // checking for all lowercase letters in the text (a - z)
        if (val > 90 && val < 97) {
            return true;
        }

        // looking for any extra ascii charaters that are in both standard ascii
        //and extended ascii.
        if (val > 122) {
            return true;
        }
        return false;
    }

    /**
     * CommandScanner is a function that is called by main, takes in user input
     * via the command line, and runs it through the DocumentScanner function.
     *
     * @throws java.io.FileNotFoundException
     */
    public static void varCommand() throws FileNotFoundException {
        /*
         setting a scanner to scan in user input.
         */

        //setting the counter for the amount of 
        //words and the amount of delimiters
        FunctionCounter.wordCounter = 0;
        FunctionCounter.delimiterCounter = 0;

    }

    
    /**
     * FunctionCounter is a global class that holds a counter for the words of a
     * document and the delimiters of a document.
     */
    public static class FunctionCounter {

        public static int wordCounter;
        public static int delimiterCounter;
    }

    /**
     * Has getters and setters for the HashMap
     */
    public static class WasScanned {
        
        // Starting position of a word inside the current string
        private int startPi;
        //Ending position of a word inside the current string
        private int endPi;
        
        //startPi and endPi are pointers in the form of getters and setters 
        private String name;
        private int scanType;

        /**
         *  syntax stuff
         */
        public enum ScanType {

            WORD, NUMBER, DELIMITER
        };

        /**
         *
         * @return return the start position of the word
         */
        int getStartPi() {

            return startPi;
        }

        /**
         *
         * @return return the end position of the word
         */
        int getEndPi() {

            return endPi;
        }

        /**
         *
         * @return the name of the word ie the word
         */
        String getName() {

            return name;
        }

        /**
         *
         * @return the scanType
         */
        int getType() {

            return scanType;
        }

        /**
         *
         * @param startPiTemp is setting the initial position of the word
         */
        void setStartPi(int startPiTemp) {
            startPi = startPiTemp;

        }

        /**
         *
         * @param endPiTemp is setting the end position of the word
         */
        void setEndPi(int endPiTemp) {

            endPi = endPiTemp;

        }

        /**
         *
         * @param nameTemp is setting the word itself
         */
        void setName(String nameTemp) {

            name = nameTemp;
        }

        /**
         *
         * @param scanTypeTemp is setting the character type
         */
        void setScanType(int scanTypeTemp) {
            scanType = scanTypeTemp;
        }
    }

    /**
     * Function that is now called by main to run the program.
     */
    public static void window() {
        WindowGUI tw = new WindowGUI();
        tw.setVisible(true);
    }

    /**
     * Creation of the GUI interface complete with button.
     */
    public static class WindowGUI extends JFrame {
        
        //shows text and button with text in a small GUI window.
        JLabel answer = new JLabel("Choose a File");
        JButton fileButton = new JButton("Open File Chooser");

        /**
         * Creates the window with the specified size
         */
        public WindowGUI() {
            /*
            Setting the width and height of the window
             */
            this.setTitle("File Chooser");
            this.setBounds(200, 300, 350, 250);
            this.getContentPane().setLayout(null);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            
            //display message to the user using this initailization step.
            this.answer.setBounds(120, 60, 100, 30);
            this.getContentPane().add(answer);

            //locatoin of the button
            this.fileButton.setBounds(100, 100, 150, 30);
            this.getContentPane().add(fileButton);
            this.fileButton.addActionListener(new ChooseButtonListener());

            

        }

        
        /**
         * Waits for user input in the form of a button press
         */
        private class ChooseButtonListener implements ActionListener {

            /**
             * 
             * @param e is the event of an action happening 
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                //prompting messages to bedisplayed to the user
                System.out.println("Hit the choose file button");
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
                System.out.println("I created the file chooser");
                int chooserSuccess = chooser.showOpenDialog(null);
                if (chooserSuccess == 0) {
                    System.out.println("I opended the file chooser. ");
                } else {
                    System.out.println("Could not open the file chooser.");
                }

                if (chooserSuccess == JFileChooser.APPROVE_OPTION) {
                    //using a try-catch block to catch a file not found exception
                    try {
                        File chosenFile = chooser.getSelectedFile();

                        // Passing the file chosen into function document scanner
                        String filePathName = chosenFile.getAbsolutePath();

                        // Remember what the standard output streem is
                        PrintStream stdout = System.out;

                        // Set the output to the scannerOutput.txt file
                        PrintStream fileStream = new PrintStream("scannerOutput.txt");

                        // Set the system output to this new file stream
                        System.setOut(fileStream);

                        // Scan the document
                        documentScanner(filePathName);

                        // restore the standard output
                        System.setOut(stdout);

                        
                        System.out.println("Finished Document Scan.");
                        System.out.println("You chose the file and path "
                                + chosenFile.getAbsolutePath());
                        System.out.println("Output went to scannerOutput.txt in the current directory."
                                + chosenFile.getName());
                        //catching the file not foind exception
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(WordCounter1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    //alternate message to be displayed if the user changed 
                    //their mind on or hit the cancel button by accident.
                    System.out.println("You hit cancel");
                }
            }
        }
    }
}
